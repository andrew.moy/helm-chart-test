FROM python:3-slim

COPY . /app

WORKDIR /app

RUN pip3 install --no-cache-dir Flask

EXPOSE 5000

CMD ["python3", "app.py", "runserver", "0.0.0.0:5000"]
